const express = require("express");
const router = express.Router();

const  permission = require("../lib/permission");
const auth = require("../lib/auth");
const controller = require("../controller/user.controller");

/* GET users listing. */
router.get("/", [auth.validateUser, permission.check("P_LISTING_USER")], controller.getAllUsr);
router.get("/me", [auth.validateUser], controller.getProfile);
router.put("/me", [auth.validateUser], controller.updateProfile);
router.get("/:id", [auth.validateUser, permission.check("P_VIEW_USER")], controller.getUsrById);
router.post("/", [auth.validateUser, permission.check("P_CREATE_USER")], controller.createUser);
router.put("/:id", [auth.validateUser, permission.check("P_EDIT_USER")], controller.updateUser);
module.exports = router;
