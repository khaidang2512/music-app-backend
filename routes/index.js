const express = require("express");
const router = express.Router();

const storage = require("../lib/storage");
const logger = require("../lib/winston");
const {exec} = require("child_process");
const LibUtils = require("../lib/LibUtils");
const config = require("../lib/config");
/* GET home page. */
router.get("/", function (req, res, next) {
    return res.json({success: true, message: "OK"});
});

//POST method route for uploading file
router.post(
    "/post_file",
    storage.upload.fields([{name: "audio", maxCount: 1}, {name: "avatar", maxCount: 1}]),
    async (req, res, next) => {
        let avatar = req.files["avatar"][0];
        let audio = req.files["audio"][0];
        //Multer middleware adds file(in case of single file ) or files(multiple files) object to the request object.
        //req.file is the demo_file
        /*        req.file.filename = config.file_config.user_image + req.file.filename;
        let isUploaded = await storage
            .uploadFile(req.file.path, req.file.filename, res)
            .then((val) => val);*/
        let isUploaded = false;
        return res.json(isUploaded);
    }
);

router.get("/test-encrypt", (req, res) => {
    return res.json({
        text: "/user/image/khải",
        encrypted: LibUtils.encryptStr("/user/image/khải"),
        decrypted: LibUtils.decryptStr(LibUtils.encryptStr("/user/image/khải")),
    });
});

router.get("/stream-mp3", async (req, res) => {
    let data = await storage
        .retrieveFile("/sample.mp3")
        .then((val) => val);
    res.set("content-type", "audio/mp3");
    res.set("accept-ranges", "bytes");
    res.end(data.Body, "binary");
});

module.exports = router;
