const express = require("express");
const router = express.Router();

const permission = require("../lib/permission");
const auth = require("../lib/auth");
const controller = require("../controller/entertainment.controller");
const storage = require("../lib/storage");

// Song routing table;
router.get("/song", [auth.getUsrInfo], controller.getAllSong);
router.get("/song/stream", [auth.getUsrInfo], controller.streamMp3);
router.get("/image/stream", [auth.getUsrInfo], controller.streamImage);
router.get("/song/:id", [auth.getUsrInfo], controller.getSongBySongId);
router.post("/song", [auth.getUsrInfo, storage.upload.fields([{name: "audio", maxCount: 1}, {
    name: "image",
    maxCount: 1
}])], controller.createSong);
router.put("/song/:id", [auth.getUsrInfo], controller.updateSong);

// Artist routing table;
router.get("/artist", [auth.getUsrInfo], controller.getAllArtist);
router.get("/artist/:id", [auth.getUsrInfo], controller.getArtistByArtistId);
router.post("/artist", [auth.getUsrInfo], controller.createArtist);
router.put("/artist/:id", [auth.getUsrInfo], controller.updateArtist);

// Category routing table;
router.get("/category", [auth.getUsrInfo], controller.getAllCategory);
router.get("/category/:id", [auth.getUsrInfo], controller.getCategoryByCategoryId);
router.post("/category", [auth.getUsrInfo], controller.createCategory);
router.put("/category/:id", [auth.getUsrInfo], controller.updateCategory);
router.put("/category/:id/add-song", [auth.getUsrInfo], controller.addSongToCategory);
router.delete("/category/:id/remove-song", [auth.getUsrInfo], controller.rmSongFromCategory);

// Playlist routing table;
router.get("/playlist", [auth.getUsrInfo], controller.getAllPlaylist);
router.get("/playlist/:id", [auth.getUsrInfo], controller.getPlaylistByPlaylistId);
router.post("/playlist", [auth.getUsrInfo], controller.createPlaylist);
router.put("/playlist/:id", [auth.getUsrInfo], controller.updatePlaylist);
router.put("/playlist/:id/add-song", [auth.getUsrInfo], controller.addSongToPlaylist);
router.delete("/playlist/:id/remove-song", [auth.getUsrInfo], controller.rmSongFromPlaylist);
module.exports = router;
