const express = require("express");
const router = express.Router();

const authController = require("../controller/auth.controller");

router.post("/", authController.oauth);

router.post("/reset-password", authController.resetPassword);

router.post("/check-code", authController.checkOtpCode);

router.post("/update-password", authController.updatePass);


module.exports = router;
