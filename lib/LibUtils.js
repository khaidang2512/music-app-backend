/* eslint-disable no-prototype-builtins */
const crypto = require("crypto");
const config = require("./config");
let algorithm = "aes-256-cbc",
    ENCRYPTION_KEY = "t6w9z$C&F)J@McQfTjWnZr4u7x!A%D*G",
    IV_LENGTH = 16; // For AES, this is always 16
;
const http = require("http");

const LibUtils = (module.exports = class LibUtils {
    static isEmpty(obj) {
        if (obj !== undefined && obj !== null && obj !== "") {
            return false;
        }
        return true;
    }

    static sha256(str) {
        return crypto.createHash("sha256").update(str).digest("hex");
    }

    static compStr(str1, str2) {
        if (str1 === str2) {
            return true;
        }
        return false;
    }

    static generate(length) {
        let result = "";
        let characters =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        let charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            result += characters.charAt(
                Math.floor(Math.random() * charactersLength)
            );
        }
        return result;
    }

    static validValue(value, defaultValue) {
        if (this.isEmpty(value)) {
            return defaultValue;
        }
        return value;
    }

    static cloneObject(obj, exCludeField = []) {
        let clone = {};
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                //ensure not adding inherited props
                let prop = exCludeField.find((item, index) => {
                    return item == key;
                });
                if (!LibUtils.isEmpty(prop)) {
                    continue;
                }
                clone[key] = obj[key];
            }
        }
        return clone;
    }

    static clearObject(obj) {
        for (let key in obj) {
            obj[key] = null;
        }
    }

    static findObj(obj, key = null, value) {
        let str = "";
        str = LibUtils.isEmpty(key) ? "" : +"." + key + ".";
        for (let key in obj) {
            if (!(obj[key] instanceof Date)) {
                if (typeof obj[key] == "object") {
                    this.findObj(obj[key]);
                }
            }
        }
    }

    //Select only specific field for update
    static updateObj(objOld, objNew, fieldAllowNull = [], exCludeField = []) {
        let updateField = {};
        updateField = objOld;
        for (let key in objOld) {
            if (objNew.hasOwnProperty(key)) {
                //Check field is allow null
                let field = fieldAllowNull.find((item) => {
                    return item == key;
                });
                if (!LibUtils.isEmpty(field)) {
                    updateField[key] = objNew[key];
                } else {
                    updateField[key] = LibUtils.isEmpty(objNew[key])
                        ? updateField[key]
                        : objNew[key];
                }
            }
        }
        let finalUpField = {};
        for (let key in updateField) {
            let prop = exCludeField.find((item, index) => {
                return item == key;
            });
            if (!LibUtils.isEmpty(prop)) {
                continue;
            }
            finalUpField[key] = updateField[key];
        }
        return finalUpField;
    }

    static generateNum(length) {
        return Math.floor(
            Math.pow(10, length - 1) +
            Math.random() * 9 * Math.pow(10, length - 1)
        );
    }

    static encryptStr(text) {
        let iv = crypto.randomBytes(IV_LENGTH);
        let cipher = crypto.createCipheriv('aes-256-cbc', new Buffer.from(ENCRYPTION_KEY), iv);
        let encrypted = cipher.update(text);
        encrypted = Buffer.concat([encrypted, cipher.final()]);
        return Buffer.from(iv.toString('hex') + ':' + encrypted.toString('hex')).toString("base64");
    }

    static decryptStr(text) {
        text = Buffer.from(text, 'base64').toString('utf-8');
        let textParts = text.split(':');
        let iv = new Buffer.from(textParts.shift(), 'hex');
        let encryptedText = new Buffer.from(textParts.join(':'), 'hex');
        let decipher = crypto.createDecipheriv('aes-256-cbc', new Buffer.from(ENCRYPTION_KEY), iv);
        let decrypted = decipher.update(encryptedText);
        decrypted = Buffer.concat([decrypted, decipher.final()]);
        return decrypted.toString();
    }

    static responseError(err, res = null) {
        return {success: false, message: err};
    }

    static statusCode(code) {
        let status = http.STATUS_CODES;
        for (let key in status) {
            if (parseInt(code) === parseInt(key)) {
                return code + " " + status[key];
            }
        }
    }
});
