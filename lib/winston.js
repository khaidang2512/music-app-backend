var appRoot = '.';
var winston = require('winston');

// define the custom settings for each transport (file, console)
var options = {
    file: {
        level: 'info',
        handleExceptions: true,
        filename: `${appRoot}/logs/app.log`,
        maxsize: 5242880, // 5MB
        maxFiles: 5,
        tailable: true,
    },
    console: {
        level: 'debug',
        handleExceptions: true,
    },
};

// instantiate a new Winston Logger with the settings defined above
// https://github.com/winstonjs/winston/blob/master/examples/quick-start.js
var logger = winston.createLogger({
    format: winston.format.combine(
        winston.format.timestamp(),
        winston.format.printf(i => `${i.timestamp} [${i.level}] :: ${i.message}`),
        winston.format.errors({stack: true})
        // https://github.com/winstonjs/winston/issues/1388
        //winston.format.printf(i => winston.format.colorize().colorize(i.level,    `${i.timestamp} [${i.level}] :: ${i.message}`))
    ),
    transports: [
        //new winston.transports.File(options.file),
        new winston.transports.Console(options.console),
        /*
        new winston.transports.Console({
            level: 'info',
            format: winston.format.combine(
                winston.format.colorize(),
                winston.format.simple()
            )
        })
         */
    ],
    exitOnError: false, // do not exit on handled exceptions
});

// create a stream object with a 'write' function that will be used by `morgan`
logger.stream = {
    write: function (message) {
        //logger.info(message);
        logger.info(message.substring(0, message.lastIndexOf("\n")));
    },
};

module.exports = logger;
