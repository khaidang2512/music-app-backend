const AWS = require("aws-sdk");
const jwt = require("jsonwebtoken");
const crypto = require("crypto");
// const usrProvider = require("../db/provider/UserProvider");
const LibUtils = require("./LibUtils");
const prKey = require("./key/privateKey");
const config = require('./config');

exports.validateUser = (req, res, next) => {
    jwt.verify(req.headers["authorization"], prKey, function (err, decoded) {
        if (err) {
            res.status(401);
            res.json({
                data: {
                    status: "401 UNAUTHORIZED",
                    timestamp: new Date().toISOString(),
                    message: "Unauthorized",
                    extra: err.message,
                },
                status: false
            });
        } else {
            // add user id to request
            req.body.userId = decoded.userId;
            next();
        }
    });
};

exports.getUsrInfo = (req, res, next) => {
    jwt.verify(req.headers["authorization"], prKey, function (err, decoded) {
        if (decoded) {
            req.body.userId = decoded.userId;
        } else {
            req.body.userId = null;
        }
        next();
    });
};
