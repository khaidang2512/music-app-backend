"use strict";

require("dotenv-safe").config({
    allowEmptyValues: true,
    example: process.env.CI ? ".env.ci.example" : ".env.example",
});

const os = require("os");
const fs = require("fs");

/* eslint-disable no-undef */
const db_info = {
    host: process.env.DB_HOST,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    port: process.env.DB_PORT,
    dialect: process.env.DB_DIALECT,
    dialectOptionsSsl: process.env.DB_DIALECT_OPTIONS_SSL,
};

const email_server = {
    host: "email-smtp.ap-southeast-2.amazonaws.com",
    port: 465,
    user: "AKIA3DIJEQZWX4DHWBMY",
    pass: "BIsFyqbKMZdOCX9ViGtmOqVgcGXqoeyxzDd1UFC4/b0Z",
};

const JWT_Expire = process.env.JWT_TOKEN_EXPIRE;

const AWS_ID_CONFIG = {
    accessKeyId: process.env.AWS_ACCESSS_KEY || "",
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY || "",
    signatureVersion: "v4",
};

const web_host = {
    host: "",
    port: null,
    protocal: "",
};

const region = {
    //singapore
    sg: "ap-southeast-1",
    //Australia
    au: "ap-southeast-2",
    //Us
    us: "us-east-1",
};

const S3_DT = {
    bucketName: process.env.S3_BUCKET_NAME,
    bucketKey: process.env.S3_BUCKET_KEY,
};

const AWS_EMAIL_SOURCE = "";

const file_config = {
    user: "/users",
    user_image: "/users/images",
    entertainment: "/entertainment",
    song: "/entertainment/song",
    song_image: "/entertainment/image",
};

const ALLOW_AUDIO = [".mp3", ".ogg", ".wav"];
const ALLOW_IMAGE = [".png", ".jpg", ".jpeg"];
module.exports = {
    db_info: db_info,
    JWT_Expire: JWT_Expire,
    AWS_ID_CONFIG: AWS_ID_CONFIG,
    region: region,
    AWS_EMAIL_SOURCE: AWS_EMAIL_SOURCE,
    web: web_host,
    email: email_server,
    S3_DT: S3_DT,
    file_config: file_config,
    ALLOW_AUDIO: ALLOW_AUDIO,
    ALLOW_IMAGE: ALLOW_IMAGE
};
