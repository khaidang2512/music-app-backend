/* eslint-disable no-case-declarations */
/* eslint-disable no-extra-boolean-cast */
const _authProvider = require("../db/provider/AuthorizationProvider");
const LibUtils = require("./LibUtils");

module.exports.check = permission => {
    return async (req, res, next) => {
        let userId = req.body.userId;
        let lsPer = [];
        lsPer = await _authProvider.getPerOfUser(userId);
        let find = null;
        lsPer.forEach((item, index) => {
            if (item.name === permission) {
                find = item.name;
            }
        });
        if (!find) {
            return await res.status(403).json({
                success: false,
                data: {
                    status: LibUtils.statusCode(403),
                    timestamp: new Date(),
                    message: "You don't have permission to perform this action!!!",
                }
            });
        }
        next();
    };
};
