/* eslint-disable no-console */
const Sequelize = require("sequelize");
const config = require("./config");
const Op = Sequelize.Op;

const operatorsAliases = {
    $eq: Op.eq,
    $ne: Op.ne,
    $gte: Op.gte,
    $gt: Op.gt,
    $lte: Op.lte,
    $lt: Op.lt,
    $not: Op.not,
    $in: Op.in,
    $notIn: Op.notIn,
    $is: Op.is,
    $like: Op.like,
    $notLike: Op.notLike,
    $iLike: Op.iLike,
    $notILike: Op.notILike,
    $regexp: Op.regexp,
    $notRegexp: Op.notRegexp,
    $iRegexp: Op.iRegexp,
    $notIRegexp: Op.notIRegexp,
    $between: Op.between,
    $notBetween: Op.notBetween,
    $overlap: Op.overlap,
    $contains: Op.contains,
    $contained: Op.contained,
    $adjacent: Op.adjacent,
    $strictLeft: Op.strictLeft,
    $strictRight: Op.strictRight,
    $noExtendRight: Op.noExtendRight,
    $noExtendLeft: Op.noExtendLeft,
    $and: Op.and,
    $or: Op.or,
    $any: Op.any,
    $all: Op.all,
    $values: Op.values,
    $col: Op.col
};

const sequelize = new Sequelize(
    config.db_info.database,
    config.db_info.user,
    config.db_info.password,
    {
        host: config.db_info.host,
        port: config.db_info.port,
        // logging: console.log,
        logging: false,
        // logging: msg => logger.debug(msg),
        maxConcurrentQueries: 100,
        dialect: config.db_info.dialect,
        dialectOptions: {
            ssl: config.db_info.dialectOptionsSsl,
        },
        pool: {
            // idle: process.env.NODE_ENV === "test" ? 3000 : 30000,
            handleDisconnects: true,
            // maxConnections: 50,
            // maxIdleTime: 30000,
            // acquire: 5000,
            maxConnections: 20,
            maxIdleTime: 30,
            max: 5,
            min: 0,
            acquire: 5000,
            // idle: process.env.NODE_ENV === 'test' ? 3000 : 30000
        },
        language: "en",
        define: {
            timestamps: false,
        },
        operatorsAliases: operatorsAliases,
    }
);

module.exports = sequelize;
