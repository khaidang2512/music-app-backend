const multer = require("multer");
const AWS = require("aws-sdk");
const fs = require("fs");
const logger = require("./winston");
const config = require("./config");
const util = require("util");
const readFilePromise = util.promisify(fs.readFile);
const path = require("path");

// configuring the DiscStorage engine.
const storage = multer.diskStorage({
    destination: "/tmp/",
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    },
});
// Use memory stream
const storage_v2 = multer.memoryStorage();
const upload = multer({storage: storage});

AWS.config.update(config.AWS_ID_CONFIG);

//Creating a new instance of S3:
const s3 = new AWS.S3();

const s3PutObject = util.promisify(s3.putObject);

//The uploadFile function
const uploadFile = async (source, targetName, policy = []) => {
    if (policy) {
        if (!policy.includes(path.extname(targetName).toLowerCase())) {
            return {
                success: false,
                message: "File upload not follow policy",
            };
        }
    }
    logger.info("preparing to upload...");
    let _source = source;
    let file = await readFilePromise(source)
        .then((val) => {
            return val;
        })
        .catch((err) => {
            logger.error({
                err: err,
            });
            return null;
        });
    if (!file) {
        return {
            success: false,
            message: "Can't get stream data",
        };
    }
    const putParams = {
        Bucket: config.S3_DT.bucketName,
        Key: config.S3_DT.bucketKey + targetName,
        Body: file,
    };
    let ckUpload = await s3
        .putObject(putParams)
        .promise()
        .then((val) => {
            console.log(val);
            return {success: true, message: "Upload data successfully"};
        })
        .catch((err) => {
            return {
                success: false,
                message: "Upload data unsuccessfully",
            };
        });
    return ckUpload;
};

//The retrieveFile function
const retrieveFile = async (filename = "", res) => {
    filename = config.S3_DT.bucketKey + filename;
    if (filename[0] === "/")
    {
        filename = filename.slice(1);
    }
    const getParams = {
        Bucket: config.S3_DT.bucketName,
        Key: filename,
    };
    return await s3
        .getObject(getParams)
        .promise()
        .then((val) => {
            return val;
        });
};

module.exports = {
    upload: upload,
    uploadFile: uploadFile,
    retrieveFile: retrieveFile,
};
