const jwt = require("jsonwebtoken");
const LibUtils = require("../lib/LibUtils");
const prKey = require("../lib/key/privateKey");
const config = require("../lib/config");
const logger = require("../lib/winston");

const usrProvider = require("../db/provider/UserProvider");

// Payload
/**
 * {
 *      "username": $username,
 *      "password": $password
 * }
 */
module.exports.oauth = async (req, res) => {
    try {
        let form = req.body;
        if (LibUtils.isEmpty(form)) {
            return await res.json({
                status: "error",
                message: "Email or password is empty",
            });
        }
        let check = await usrProvider
            .getUsrByEmail(form.username)
            .then((val) => {
                return val;
            })
            .catch((err) => {
                return null;
            });
        if (LibUtils.isEmpty(check)) {
            return await res.json({
                status: "error",
                message: "Email or Password is not correct. Please try again!",
            });
        }
        form.password = LibUtils.sha256(form.password); //crypto.createHash('sha256').update(form.password).digest('hex');
        if (form.password !== check.password) {
            return await res.json({
                status: "error",
                message: "Email or Password is not correct. Please try again!",
            });
        }
        if (form.active == 1) {
            return await res.json({
                status: "error",
                message: "User is inactive yet!!!",
            });
        }
        const token = jwt.sign(
            {
                userId: check.id,
            },
            prKey,
            {
                expiresIn: config.JWT_Expire,
            }
        );
        return await res.json({
            status: "success",
            type: "Bearer",
            token: token,
            ttl: config.JWT_Expire,
        });
    } catch (error) {
        return await res.json({
            status: "error",
            message: "Something wrong happen",
        });
    }
};

module.exports.resetPassword = async (req, res) => {
    let email = req.body.email;
    let val = await usrProvider.requestOtp(email)
        .then(val => val)
        .catch(err => LibUtils.responseError(err));
    return res.json(val);
};

module.exports.checkOtpCode = async (req, res) => {
    let code = req.body.code;
    let val = await usrProvider.validOtpCode(code)
        .then(val => val)
        .catch(err => LibUtils.responseError(err));
    return res.json(val);
};

module.exports.updatePass = async (req, res) => {
    let code = req.body.code, newPass = req.body.newPass;
    let val = await usrProvider.changePass(code, newPass)
        .then(val => val)
        .catch(err => LibUtils.responseError(err));
    return res.json(val);
};

