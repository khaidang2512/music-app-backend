const jwt = require("jsonwebtoken");
const LibUtils = require("../lib/LibUtils");
const prKey = require("../lib/key/privateKey");
const config = require("../lib/config");
const logger = require("../lib/winston");
const storage = require("../lib/storage");
const path = require("path");

const _provider = require("../db/provider/EntertainmentProvider");

module.exports.getAllSong = async (req, res, next) => {
    let lstSong = await _provider.getAllSong().then(val => {
        let lstResult = [];
        if (val) {
            val.forEach(item => {
                item.path = "/entmt/song/stream?key=" + LibUtils.encryptStr(item.path || null);
                item.image = "/entmt/image/stream?key=" + LibUtils.encryptStr(item.image || null);
                lstResult.push(item);
            });
        }
        return lstResult;
    });
    return res.json({
        success: true,
        data: lstSong
    });
};

module.exports.getSongBySongId = async (req, res, next) => {
    let songId = req.params.id;
    let songObj = await _provider.getSongById(songId);
    if (!songObj) {
        return res.json({
            success: false,
            message: "Can't found song"
        });
    }
    songObj.path = "/entmt/song/stream?key=" + LibUtils.encryptStr(songObj.path || null);
    songObj.image = "/entmt/image/stream?key=" + LibUtils.encryptStr(songObj.image || null);
    return res.json({
        success: true,
        data: songObj
    });
};

module.exports.createSong = async (req, res, next) => {
    let songObj = req.body;
    let userId = req.body.userId || null;
    let image = (req.files["image"]) ? (req.files["image"][0]) : (null);
    let audio = (req.files["audio"]) ? (req.files["audio"][0]) : (null);
    if (!audio) {
        return res.json({success: false, message: "Audio is empty"});
    }
    audio.filename = config.file_config.song + "/" + audio.filename;
    image.filename = (image) ? (config.file_config.song_image + "/" + image.filename) : (null);
    songObj.path = audio.filename;
    songObj.image = image.filename;
    let result = await _provider.createSong(songObj);
    let uploadAudio = await storage.uploadFile(audio.path, audio.filename, config.ALLOW_AUDIO)
        .catch(err => {
            return {success: false, message: "Can't upload audio file to s3"};
        });
    if (!uploadAudio.success) {
        return res.json(uploadAudio);
    }
    let uploadImg = {};
    if (image) {
        uploadImg = await storage.uploadFile(image.path, image.filename, config.ALLOW_IMAGE)
            .catch(err => {
                return {success: false, message: "Can't upload image file to s3"};
            });
        if (!uploadImg.success) {
            return res.json(uploadAudio);
        }
    }
    if (!result) {
        return res.json({
            success: false,
            message: "Can't create song"
        });
    }
    result.path = "/entmt/song/stream?key=" + LibUtils.encryptStr(result.path || null);
    result.image = "/entmt/image/stream?key=" + LibUtils.encryptStr(result.image || null);
    return res.json(({
        success: true,
        message: "create song successfully",
        data: result
    }));
};

module.exports.updateSong = async (req, res, next) => {
    let songId = req.params.id;
    let songObj = req.body;
    if (isNaN(songId)) {
        next(new Error("Invalid number"));
        return;
    }
    let result = await _provider.updateSong(songId, songObj);
    if (result) {
        result.path = "/entmt/song/stream?key=" + LibUtils.encryptStr(result.path || null);
        return res.json(
            {
                success: true,
                message: "Update song successfully",
                data: result
            });
    }
    return res.json({
        success: false,
        message: "Update artist unsuccessfully"
    });
};

module.exports.getAllArtist = async (req, res, next) => {
    let lstArtist = await _provider.getAllArtist();
    return res.json({
        success: true,
        data: lstArtist
    });
};

module.exports.getArtistByArtistId = async (req, res, next) => {
    let artistId = req.params.id;
    let artistObj = await _provider.getArtistById(artistId);
    if (!artistObj) {
        return res.json({
            success: false,
            message: "Can't found artist"
        });
    }

    return res.json({
        success: true,
        data: artistObj
    });
};

module.exports.createArtist = async (req, res, next) => {
    let artistObj = req.body;
    let result = await _provider.createArtist(artistObj);
    if (result) {
        return res.json(
            {
                success: true,
                message: "Create artist successfully",
                data: result
            });
    }
    return res.json({
        success: false,
        message: "Create artist unsuccessfully"
    });
};

module.exports.updateArtist = async (req, res, next) => {
    let artistObj = req.body;
    let artistId = req.params.id;
    if (isNaN(artistId)) {
        next(new Error("Invalid number"));
        return;
    }
    let result = await _provider.updateArtist(artistId, artistObj);
    if (result) {
        return res.json(
            {
                success: true,
                message: "Update artist successfully",
                data: result
            });
    }
    return res.json({
        success: false,
        message: "Update artist unsuccessfully"
    });
};

module.exports.getAllCategory = async (req, res, next) => {
    let lstCategory = await _provider.getAllCategory();
    return res.json({
        success: true,
        data: lstCategory
    });
};

module.exports.getCategoryByCategoryId = async (req, res, next) => {
    let categoryId = req.params.id;
    let categoryObj = await _provider.getListSongByCategory(categoryId).then(val => {
        if (val) {
            let lstResult = [];
            val.song.forEach(item => {
                let newItem = LibUtils.cloneObject(item);
                newItem.path = "/entmt/song/stream?key=" + LibUtils.encryptStr(item.path || null);
                lstResult.push(newItem);
            });
            val.song = lstResult;
        }
        return val;
    });
    if (!categoryObj) {
        return res.json({
            success: false,
            message: "Can't found artist"
        });
    }

    return res.json({
        success: true,
        data: categoryObj
    });
};

module.exports.createCategory = async (req, res, next) => {
    let categoryObj = req.body;
    let userId = req.body.userId || null;
    let lstSongId = req.body.song || [];
    let result = await _provider.createCategory(categoryObj, lstSongId, userId);
    if (result) {
        return res.json(
            {
                success: true,
                message: "Create category successfully",
                data: result
            });
    }
    return res.json({
        success: false,
        message: "Create category unsuccessfully"
    });
};

module.exports.updateCategory = async (req, res, next) => {
    let categoryObj = req.body;
    let categoryId = req.params.id;
    if (isNaN(categoryId)) {
        next(new Error("Invalid number"));
        return;
    }
    let result = await _provider.updateCategory(categoryId, categoryObj);
    if (result) {
        return res.json(
            {
                success: true,
                message: "Update category successfully",
                data: result
            });
    }
    return res.json({
        success: false,
        message: "Update category unsuccessfully"
    });
};

module.exports.addSongToCategory = async (req, res, next) => {
    let lstSongId = req.body.song || [];
    let categoryId = req.params.id;
    let userId = req.body.userId || null;
    let result = await _provider.addSongToCategory(categoryId, lstSongId, userId);
    if (result) {
        return res.json(
            {
                success: true,
                message: "Add song to category successfully",
                data: result
            });
    }
    return res.json({
        success: false,
        message: "Add song to  category unsuccessfully"
    });
};

module.exports.rmSongFromCategory = async (req, res, next) => {
    let lstSongId = req.body.song || [];
    let categoryId = req.params.id;
    let result = await _provider.rmSongFromCategory(categoryId, lstSongId);
    return res.json(
        {
            success: true,
            message: "Remove song from category successfully",
        });
};

module.exports.getAllPlaylist = async (req, res, next) => {
    let lstPlaylist = await _provider.getAllPlaylist();
    return res.json({
        success: true,
        data: lstPlaylist
    });
};

module.exports.getPlaylistByPlaylistId = async (req, res, next) => {
    let playlistId = req.params.id;
    let playlistObj = await _provider.getListSongByPlaylistId(playlistId).then(val => {
        if (val) {
            let lstResult = [];
            val.song.forEach(item => {
                let newItem = LibUtils.cloneObject(item);
                newItem.path = "/entmt/song/stream?key=" + LibUtils.encryptStr(item.path || null);
                lstResult.push(newItem);
            });
            val.song = lstResult;
        }
        return val;
    });
    if (!playlistObj) {
        return res.json({
            success: false,
            message: "Can't found artist"
        });
    }

    return res.json({
        success: true,
        data: playlistObj
    });
};

module.exports.createPlaylist = async (req, res, next) => {
    let playlistObj = req.body;
    let userId = req.body.userId || null;
    let lstSongId = req.body.song || [];
    let result = await _provider.createPlaylist(playlistObj, lstSongId, userId);
    if (result) {
        return res.json(
            {
                success: true,
                message: "Create playlist successfully",
                data: result
            });
    }
    return res.json({
        success: false,
        message: "Create playlist unsuccessfully"
    });
};

module.exports.updatePlaylist = async (req, res, next) => {
    let playlistObj = req.body;
    let playlistId = req.params.id;
    if (isNaN(playlistId)) {
        next(new Error("Invalid number"));
        return;
    }
    let result = await _provider.updatePlaylist(playlistObj, categoryObj);
    if (result) {
        return res.json(
            {
                success: true,
                message: "Update playlist successfully",
                data: result
            });
    }
    return res.json({
        success: false,
        message: "Update playlist unsuccessfully"
    });
};

module.exports.addSongToPlaylist = async (req, res, next) => {
    let lstSongId = req.body.song || [];
    let playlistId = req.params.id;
    let userId = req.body.userId || null;
    let result = await _provider.addSongToPlaylist(playlistId, lstSongId, userId);
    if (result) {
        return res.json(
            {
                success: true,
                message: "Add song to playlistId successfully",
                data: result
            });
    }
    return res.json({
        success: false,
        message: "Add song to  playlistId unsuccessfully"
    });
};

module.exports.rmSongFromPlaylist = async (req, res, next) => {
    let lstSongId = req.body.song || [];
    let playlistId = req.params.id;
    let result = await _provider.rmSongFromPlaylist(playlistId, lstSongId);
    return res.json(
        {
            success: true,
            message: "Remove song from playlist successfully",
        });
};

module.exports.streamMp3 = async (req, res, next) => {
    let key = req.query.key || null;
    key = LibUtils.decryptStr(key);
    let data = await storage
        .retrieveFile(key)
        .then((val) => val);
    let ext = path.extname(key||'').split('.');
    let extension =  ext[ext.length - 1];
    res.set("content-type", "audio/" + extension);
    res.set('Content-Disposition', 'attachment; filename=' + path.basename(key));
    /*    res.setHeader('filename=' + path.basename(key));*/
    res.set("accept-ranges", "bytes");
    return res.end(data.Body, "binary");
};

module.exports.streamImage = async (req, res, next) => {
    let key = req.query.key || null;
    key = LibUtils.decryptStr(key);
    let data = await storage
        .retrieveFile(key)
        .then((val) => val);
    let ext = path.extname(key||'').split('.');
    let extension =  ext[ext.length - 1];
    res.set("content-type", "image/" + extension);
    res.set('Content-Disposition', 'filename=' + path.basename(key));
    /*    res.set("content-type", "audio/mp3");*/
    res.set("accept-ranges", "bytes");
    return res.end(data.Body, "binary");
};
