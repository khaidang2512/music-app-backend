const userProvider = require("../db/provider/UserProvider");

exports.getAllUsr = async (req, res, next) => {
    userProvider.getAllUser().then((val) => {
        return res.json({
            success: true,
            data: val,
        });
    });
};

exports.createUser = async (req, res, next) => {
    let usr = req.body;
    usr.is_active = usr.is_active || 1;
    userProvider
        .createUsr(usr)
        .then((val) => {
            return res.json(val);
        })
        .catch((err) => {
            return res.json({
                success: false,
                message: "Create user unsuccessfully",
            });
        });
};

exports.updateUser = async (req, res, next) => {
    let user = req.body;
    let userId = req.params.id;
    if (isNaN(userId)) {
        next(new Error("Invalid number"));
        return;
    }
    userProvider
        .updateUsr(userId, user)
        .then((val) => {
            return res.json(val);
        })
        .catch((err) => {
            return res.json({
                success: false,
                message: "Update user unsuccessfully",
            });
        });
};

exports.getUsrById = async (req, res, next) => {
    let userId = req.params.id;
    if (isNaN(userId)) {
        next(new Error("Invalid number"));
        return;
    }
    await userProvider
        .getUsrById(userId)
        .then((val) => {
            return res.json({
                success: true,
                data: val,
            });
        })
        .catch((err) => {
            return res.json({
                success: false,
                message: "Update user unsuccessfully",
            });
        });
};

exports.getProfile = async (req, res, next) => {
    let userId = req.body.userId;
    return res.json(await userProvider
        .getUsrById(userId, true, true)
        .then(val => {
            return {
                success: true,
                data: val,
            };
        })
        .catch(err => {
            return {
                success: false,
                message: "Update user unsuccessfully",
            };
        }));
};

exports.updateProfile = async (req, res, next) => {
    let userId = req.body.userId;
    if (isNaN(userId)) {
        next(new Error("Invalid number"));
        return;
    }
    let payload = req.body;
    return res.json(await userProvider
        .updateUsr(userId, payload)
        .then(val => val)
        .catch((err) => {
            return {
                success: false,
                message: "Update user unsuccessfully",
            };
        }));
};
