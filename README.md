# Music App Backend

[![Build Status](https://gitlab.com/khaidang2512/music-app-backend/badges/release/pipeline.svg)](https://gitlab.com/khaidang2512/music-app-backend)

### Tech

Music App Backend uses a number of latest technology to work properly:

* [markdown-it] - Markdown parser done right. Fast and easy to extend.
* [node.js] - evented I/O for the backend
* [Express] - fast node.js network app framework [@tjholowaychuk]
* [AWS] - Amazon Web Services
* [Lambda] - AWS Lambda Function
* [Serverless] - AWS Express Serverless Framework
* [S3] - AWS Cloud Storge

### Installation

Music App backend requires [Node.js](https://nodejs.org/)  v12+ LTS to run.

Install the dependencies and devDependencies and start the server.

```sh
$ git clone https://gitlab.com/khaidang2512/music-app-backend.git
$ cd music-app-backend
$ npm install 
$ npm install -g nodemon #Require to be sudo or Administrator access
$ nodemon start
```
#### Author: Khai Dang (1654052054khai@ou.edu.vn)

License
----

GPLv2


**Happy Coding, Hell Yeah!**
