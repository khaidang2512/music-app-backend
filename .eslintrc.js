module.exports = {
    "parserOptions": {
        "ecmaVersion": 8,
        "sourceType": "module",
        "ecmaFeatures": {
            "jsx": true
        }
    },
    "extends": [
        "eslint:recommended",
        "plugin:node/recommended"
    ],
    "plugins": [
        "node",
        // "mocha"
    ],
    "env": {
        "browser": false,
        "node": true,
        "es6": true,
        "mocha": true
    },
    "globals": {
        "moment": true,
        "Base64": true
    },
    "rules": {
        "indent": ["error", 4],
        "no-const-assign": 1,
        "no-extra-semi": 0,
        "semi": 1,
        "no-fallthrough": 1,
        "no-empty": 1,
        "no-mixed-spaces-and-tabs": 1,
        "no-redeclare": 0,
        "no-this-before-super": 1,
        "no-undef": 1,
        "no-unreachable": 1,
        "no-unused-vars": 0,
        "no-use-before-define": 0,
        "no-console": 1,
        "constructor-super": 1,
        "no-return-await": 0,
        "curly": 1,
        "eqeqeq": 0,
        "func-names": 0,
        "valid-typeof": 1,
        "space-before-function-paren": ["error", {
            "anonymous": "always",
            "named": "never",
            "asyncArrow": "always"
        }],
        // "node/exports-style": ["error", "module.exports"],
        "node/no-unpublished-require": "off",
        // "mocha/no-exclusive-tests": "error"
    }
};
