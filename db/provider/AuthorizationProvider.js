const LibUtils = require("../../lib/LibUtils");
const Sequelize = require("sequelize").Sequelize;
const sequelize = require("../../lib/connection");

const userRoleModel = require("../model/user_role");
const userRole = userRoleModel(sequelize, Sequelize);

const roleModel = require("../model/role");
const role = roleModel(sequelize, Sequelize);

const permissionModel = require("../model/permission");
const permission = permissionModel(sequelize, Sequelize);


class AuthorizationProvider {
    async getPerOfUser(userId, isGtFull = false) {
        let attributes = "per.*";
        if (!isGtFull) {
            attributes = "per.id,per.name";
        }
        let queryString =
            "SELECT DISTINCT " +
            attributes +
            " FROM user JOIN user_role on user.id=user_role.user_id JOIN role ON role.id=user_role.role_id JOIN role_permission ON role_permission.role_id=role.id  JOIN permission per ON per.id = role_permission.permission_id WHERE user.id=" +
            userId;
        return await sequelize
            .query(
                queryString,
                {
                    type: Sequelize.QueryTypes.SELECT
                },
                {
                    raw: true
                }
            )
            .then(data => data);
    }

    async getRoleOfUser(userId, isGtFull = false) {
        let attributes = "role.*";
        if (!isGtFull) {
            attributes = "role.id, role.name";
        }
        let queryString =
            "SELECT DISTINCT " +
            attributes +
            " FROM user JOIN user_role ON user.id = user_role .user_id JOIN role ON user_role.role_id = role.id WHERE user.id=" +
            userId;
        return await sequelize
            .query(
                queryString,
                {
                    type: Sequelize.QueryTypes.SELECT
                },
                {
                    raw: true
                }
            )
            .then(data => data);
    }

    async getAllRole() {
        let result = await role.findAll({
            raw: true,
        }).then(value => value).catch(err => []);
        return result;
    }

    async getRoleById(roleId) {
        let result = await role.findOne({
            where: {
                id: roleId,
            },
            raw: true
        }).then(value => value).catch(err => []);
        return result;
    }

    async getAllPermission() {
        let result = await permission.findAll({
            raw: true,
        }).then(value => value).catch(err => []);
        return result;
    }

    async getPermissionById(permissionId) {
        let result = await permission.findOne({
            where: {
                id: permissionId,
            },
            raw: true
        }).then(value => value).catch(err => []);
        return result;
    }

}

module.exports = new AuthorizationProvider();
