/* eslint-disable no-extra-boolean-cast */
/* eslint-disable require-atomic-updates */
const crypto = require("crypto");
const LibUtils = require("../../lib/LibUtils");
const logger = require("../../lib/winston");
const Sequelize = require("sequelize").Sequelize;
const sequelize = require("../../lib/connection");
const userModel = require("../model/user");
const user = userModel(sequelize, Sequelize);

const userOtpModel = require("../model/user_otp");
const user_otp = userOtpModel(sequelize, Sequelize);

const SendEmail = require("../../lib/SendEmail");
const fs = require("fs");
const path = require("path");
const config = require("../../lib/config");
const moment = require("moment");

const authProvider = require("./AuthorizationProvider");

String.prototype.replaceAll = function (search, replacement) {
    let target = this;
    return target.replace(new RegExp(search, "g"), replacement);
};



class UserProvider {
    async getAllUser() {
        let lstUser = await user
            .findAll({
                raw: true,
            })
            .then(async (val) => {
                let output = [];
                for (let i = 0; i < val.length; i++) {
                    let obj = new Object(val[i]);
                    obj.password = "";

                    output.push(obj);
                }
                return output;
            })
            .catch((err) => {
                return [];
            });
        return lstUser;
    }

    async getUsrById(id, excludePass = true, isProfile = false) {
        let generateQuery = {
            where: {
                id: id,
            },
            raw: true,
            attributes: {},
        };
        if (excludePass) {
            generateQuery.attributes = {
                exclude: ["password"],
            };
        }
        let usr = await user
            .findOne(generateQuery)
            .then(async (val) => {
                if (isProfile) {
                    let lstPermission = await authProvider.getPerOfUser(val.id).then(val => {
                        let lstName = [];
                        val.forEach(item => {
                            lstName.push(item.name);
                        });
                        return lstName;
                    });

                    let lstRole = await authProvider.getRoleOfUser(val.id).then(val => {
                        let lstName = [];
                        val.forEach(item => {
                            lstName.push(item.name);
                        });
                        return lstName;
                    });
                    val.p = lstPermission;
                    val.r = lstRole;
                }
                return val;
            })
            .catch((err) => {
                return null;
            });
        return usr;
    }

    async getUsrByEmail(email) {
        let usr = await user
            .findOne({
                where: {
                    email: email,
                },
                raw: true,
            })
            .then((val) => {
                return val;
            })
            .catch((err) => {
                logger.error(
                    JSON.stringify({
                        method: "UserProvider#getUsrByEmail() | user#findOne()",
                        err: err,
                        email: email,
                    })
                );
                return null;
            });
        return usr;
    }

    async createUsr(usr, sendEmail = false) {
        usr.is_active = 0;
        let find = await this.getUsrByEmail(usr.email)
            .then((val) => {
                return val;
            })
            .catch((err) => {
                return null;
            });
        if (LibUtils.isEmpty(find)) {
            usr.password = LibUtils.sha256(usr.password);
            let usrObj = await user
                .create(usr)
                .then(async (val) => {
                    val.password = "";
                    return new Object({
                        success: true,
                        message: "Create user successfully", //val.toJSON(),
                        data: val,
                    });
                })
                .catch((err) => {
                    return {
                        success: "error",
                        message: "Can't insert data to DB",
                        err: JSON.stringify(err),
                    };
                });
            return usrObj;
        } else {
            return new Object({
                success: false,
                message: "Email existed!!",
            });
        }
    }

    async requestOtp(email) {
        let key = LibUtils.generateNum(10);
        let template_html = null;
        let title = null,
            text = null,
            controller = null,
            icon = null;
        template_html = fs.readFileSync(
            path.resolve(
                __dirname,
                "../../public/template/password-reset/content.html"
            ),
            "utf8"
        );
        title = "Reset Password | Music App Team";
        text = "Reset Password";
        let usr = await this.getUsrByEmail(email)
            .then((val) => {
                return val;
            })
            .catch((err) => {
                return null;
            });
        if (LibUtils.isEmpty(usr)) {
            return new Object({
                success: false,
                message: "user not found",
            });
        }

        let receiver =
            usr.firstname !== null || usr.lastname !== null
                ? usr.firstname + " " + usr.lastname
                : usr.email;
        template_html = template_html.replaceAll("{{name}}", receiver);
        template_html = template_html.replaceAll("{{email}}", usr.email);
        template_html = template_html.replaceAll("{{otp_code}}", key);
        let find = await user_otp
            .update(
                {
                    code: key,
                    updated: new Date(),
                },
                {
                    where: {
                        id: usr.id,
                    },
                }
            )
            .then((val) => {
                if (val[0] > 0) {
                    return true;
                }
                return false;
            })
            .catch((err) => {
                return null;
            });
        if (find === null) {
            return new Object({
                success: false,
                message: "Something wrong happen",
            });
        }
        if (!find) {
            let newReq = {};
            newReq.user_id = usr.id;
            newReq.code = key;
            newReq.created = new Date();
            newReq.updated = new Date();
            let result = await user_otp
                .create(newReq)
                .then(async (val) => {
                    return await SendEmail(email, title, text, template_html)
                        .then((val) => {
                            return val;
                        })
                        .catch((err) => {
                            return {
                                success: false,
                                message: "Send email unsuccessfully",
                                err: JSON.stringify(err, null, 2),
                            };
                        });
                })
                .catch((err) => {
                    return {
                        success: false,
                        message: "Invalid Email!!!",
                    };
                });
            return result;
        } else {
            return await SendEmail(email, title, text, template_html)
                .then((val) => {
                    return val;
                })
                .catch((err) => {
                    return {
                        success: false,
                        message: "Send email unsuccessfully",
                        err: JSON.stringify(err, null, 2),
                    };
                });
        }
    }

    //Validate forget key request
    async validOtpCode(key, isExport = false) {
        let find = await user_otp
            .findOne({
                where: {
                    code: key,
                },
                raw: true,
            })
            .then((val) => {
                return val;
            })
            .catch((err) => {
                return null;
            });
        if (LibUtils.isEmpty(find)) {
            return new Object({
                success: false,
                message: "Can't find code",
            });
        }
        let usr = await this.getUsrById(find.user_id).catch(err => null);
        //Current date time compare
        let cr_dt = new Date();
        let max_time = new Date(find.updated);
        max_time.setMinutes(max_time.getMinutes() + 15);
        if (cr_dt > max_time || cr_dt < find.updated) {
            // TODO: not delete record, but mark it as invalid `token_status`
            let result = await user_otp
                .destroy({
                    where: {
                        id: find.id,
                    },
                    limit: 1,
                })
                .then((val) => {
                    return {
                        success: false,
                        message: "Time out",
                    };
                });
            return result;
        }
        return new Object({
            success: true,
            message: "The code's valid",
            value: isExport ? usr : null,
        });
    }

    async changePass(key, newPass) {
        let check = await this.validOtpCode(key, true);
        if (check.status === "error") {
            return check;
        }
        let usr = check.value;
        if (LibUtils.isEmpty(usr)) {
            return new Object({
                success: false,
                message: "user not found",
            });
        }
        let result = await user
            .update(
                {
                    password: LibUtils.sha256(newPass),
                },
                {
                    where: {
                        id: usr.id,
                    },
                }
            )
            .then(async (val) => {
                // TODO: not delete record, but mark it as invalid `token_status`
                await user_otp.destroy({
                    where: {
                        id: usr.id,
                    },
                });
                return {
                    success: true,
                    message: "Update password successfully",
                };
            })
            .catch((err) => {
                return {
                    success: false,
                    message: "Nothing updated !!",
                };
            });
        return result;
    }

    async updateUsr(userId, usr) {
        let usrFind = await this.getUsrById(userId).then((val) => {
            return val;
        });
        if (!usrFind) {
            return new Object({
                success: false,
                message: "Can't find user !!",
            });
        }
        //Allow only update field if they not null, it's null keep old value
        // If can change password, then this user is active user.
        let updateField = LibUtils.updateObj(
            usrFind,
            usr,
            [],
            ["id", "password", "image", "is_active"]
        );

        let result = await user
            .update(updateField, {
                where: {
                    id: userId,
                },
                limit: 1,
            })
            .then((val) => {
                return {
                    success: true,
                    message: "Update user successfully",
                };
            })
            .catch((err) => {
                return {
                    success: false,
                    message: "Something wrong happen!!\nUser is not updated",
                };
            });
        return result;
    }

    async disableAccount(userId) {
        let _find = await user
            .findOne(
                {
                    is_active: 0,
                },
                {
                    where: {
                        id: userId,
                    },
                    limit: 1,
                }
            )
            .then((val) => {
                return {success: true, message: "Disable user successfully"};
            })
            .catch((err) => {
                return {
                    success: false,
                    message: "Something wrong happen!!\nUser is not updated",
                };
            });
    }

    async updateUsrPass(userId, oldPass, newPass) {
        let usr = this.getUsrById(userId, false).catch(err => null);
        if (!usr) {
            return LibUtils.responseError("User not found");
        }
        if (usr.password !== LibUtils.sha256(oldPass)) {
            return LibUtils.responseError("Old password not match");
        }
        let result = await user
            .update(
                {
                    password: LibUtils.sha256(newPass),
                },
                {
                    where: {
                        id: usr.id,
                    },
                }
            )
            .then(async (val) => {
                return {
                    success: true,
                    message: "Update password successfully",
                };
            })
            .catch((err) => {
                return {
                    success: false,
                    message: "Nothing updated !!",
                };
            });
        return result;
    }
}

module.exports = new UserProvider();
