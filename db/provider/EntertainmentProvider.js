const LibUtils = require("../../lib/LibUtils");
const logger = require("../../lib/winston");
const Sequelize = require("sequelize").Sequelize;
const sequelize = require("../../lib/connection");

const songModel = require("../model/song");
const song = songModel(sequelize, Sequelize);

const songCategoryModel = require("../model/song_category");
const songCategory = songCategoryModel(sequelize, Sequelize);

const songPlaylistModel = require("../model/song_playlist");
const songPlaylist = songPlaylistModel(sequelize, Sequelize);

const artistModel = require("../model/artist");
const artist = artistModel(sequelize, Sequelize);

const categoryModel = require("../model/category");
const category = categoryModel(sequelize, Sequelize);

const playlistModel = require("../model/playlist");
const playlist = playlistModel(sequelize, Sequelize);


class EntertainmentProvider {
    async getAllSong() {
        let result = await song.findAll({
            raw: true,
        }).then(async value => {
            let lstResult = [];
            if (value) {
                for (let i = 0; i < value.length; i++) {
                    let obj = value[i];
                    let artistObj = await this.getArtistById(value[i].artist_id)
                        .then(val => val)
                        .catch(err => null);
                    obj.artist = artistObj;
                    lstResult.push(obj);
                }
            }
            return lstResult;
        }).catch(err => []);
        return result;
    }

    async getSongById(songId) {
        let result = await song.findOne({
            where: {
                id: songId,
            },
            raw: true
        }).then(async value => {
            let artistObj = await this.getArtistById(value.artist_id);
            value.artist = artistObj;
            return value;
        }).catch(err => null);
        return result;
    }

    async createSong(songObj) {
        let result = await song.create(songObj).then(val => val).catch(err => {
            logger.error({
                error: "Can't create song",
                payload: JSON.stringify(songObj),
                message: JSON.stringify(err)
            });
            return null;
        });
        return result;
    }

    async updateSong(songId, updateSongObj) {
        let songObj = await this.getSongById(songId);
        if (!songObj) {
            return null;
        }
        //Allow only update field if they not null, it's null keep old value
        // If can change password, then this user is active user.
        let updateField = LibUtils.updateObj(
            songObj,
            updateSongObj,
            [],
            ["id", "image", "path"]
        );

        let result = await song
            .update(updateField, {
                where: {
                    id: songObj.id,
                },
                limit: 1,
            })
            .then(async val => {
                let fetchObj = await this.getSongById(songId);
                return  fetchObj;
            })
            .catch(err => null);
        return result;
    }

    async getAllArtist() {
        let result = await artist.findAll({
            raw: true,
        }).then(value => value).catch(err => []);
        return result;
    }

    async getArtistById(artistId) {
        let result = await artist.findOne({
            where: {
                id: artistId,
            },
            raw: true
        }).then(value => value).catch(err => null);
        return result;
    }

    async getAllPlaylist() {
        let result = await playlist.findAll({
            raw: true,
        }).then(value => value).catch(err => []);
        return result;
    }

    async createArtist(artistObj) {
        let result = await artist.create(artistObj).then(val => val).catch(err => {
            logger.error({
                error: "Can't create artist",
                payload: JSON.stringify(artistObj),
                message: JSON.stringify(err)
            });
            return null;
        });
        return result;
    }

    async updateArtist(artistId, updateArtistObj) {
        let artistObj = await this.getArtistById(artistId);
        if (!artistObj) {
            return null;
        }
        //Allow only update field if they not null, it's null keep old value
        // If can change password, then this user is active user.
        let updateField = LibUtils.updateObj(
            artistObj,
            updateArtistObj,
            [],
            ["id", "image"]
        );

        let result = await artist
            .update(updateField, {
                where: {
                    id: artistObj.id,
                },
                limit: 1,
            })
            .then(async val => {
                let fetchObj = await this.getArtistById(artistId);
                return  fetchObj;
            })
            .catch(err => null);
        return result;
    }

    async getPlaylistById(playListId) {
        let result = await playlist.findOne({
            where: {
                id: playListId,
            },
            raw: true
        }).then(value => value).catch(err => null);
        return result;
    }

    async getListSongByPlaylistId(playlistId) {
        let playlistObj = await this.getPlaylistById(playlistId);
        if (!playlistObj) {
            return null;
        }
        let songPlaylistObj = await songPlaylist.findAll({
            where: {
                playlist_id: playlistObj.id
            },
            raw: true
        }).then(async val => {
            let lstResult = [];
            for (let i = 0; i < val.length; i++) {
                let songItem = await this.getSongById(val[i].song_id);
                lstResult.push(songItem);
            }
            return lstResult;
        }).catch(err => []);
        playlistObj.song = songPlaylistObj;
        return playlistObj;
    }

    async getAllCategory() {
        let result = await category.findAll({
            raw: true,
        }).then(value => value).catch(err => []);
        return result;
    }

    async getCategoryById(categoryId) {
        let result = await category.findOne({
            where: {
                id: categoryId,
            },
            raw: true
        }).then(value => value).catch(err => null);
        return result;
    }

    async getListSongByCategory(categoryId) {
        let categoryObj = await this.getCategoryById(categoryId);
        if (!categoryObj) {
            return null;
        }
        let songPlaylistObj = await songCategory.findAll({
            where: {
                category_id: categoryObj.id
            },
            raw: true
        }).then(async val => {
            let lstResult = [];
            for (let i = 0; i < val.length; i++) {
                let songItem = await this.getSongById(val[i].song_id);
                lstResult.push(songItem);
            }
            return lstResult;
        }).catch(err => []);
        categoryObj.song = songPlaylistObj;
        return categoryObj;
    }

    // Playlist logic provider
    async addSongToPlaylist(playlistId, lstSongId = [], userId = null) {
        let playlistObj = await this.getPlaylistById(playlistId);
        if (!playlistId) {
            return [];
        }
        let lstSongCreate = [];
        for (let i = 0; i < lstSongId.length; i++) {
            let obj = {
                song_id: lstSongId[i],
                playlist_id: playlistObj.id,
                creator: userId
            };
            let resultCreate = await songPlaylist.create(obj).then(val => val).catch(err => null);
            lstSongCreate.push(resultCreate);
        }
        return lstSongCreate;
    }

    async rmSongFromPlaylist(playlistId, lstSongId = []) {
        let playlistObj = await this.getPlaylistById(playlistId);
        if (!playlistId) {
            return [];
        }
        for (let i = 0; i < lstSongId.length; i++) {
            await songPlaylist.destroy({
                where: {
                    playlist_id: playlistObj.id,
                    $and: [{
                        song_id: {
                            $eq: lstSongId[i]
                        }
                    }]
                }
            });
        }
        return true;
    }

    async createPlaylist(playlistObject, lstSongId = [], userId = null) {
        let playlistObj = await playlist.create(playlistObject)
            .then(val => val)
            .catch(err => null);
        if (!playlistObj) {
            return null;
        }
        await this.addSongToPlaylist(playlistObj.id, lstSongId, userId);
        return playlistObj;
    }

    async updatePlaylist(playlistId, updatePlaylistObj) {
        let playlistObj = await this.getPlaylistById(playlistId);
        if (!playlistObj) {
            return null;
        }
        //Allow only update field if they not null, it's null keep old value
        // If can change password, then this user is active user.
        let updateField = LibUtils.updateObj(
            playlistObj,
            updatePlaylistObj,
            [],
            ["id", "image"]
        );

        let result = await playlist
            .update(updateField, {
                where: {
                    id: playlistObj.id,
                },
                limit: 1,
            })
            .then(async val => {
                let fetchObj = await this.getPlaylistById(playlistId);
                return  fetchObj;
            })
            .catch(err => null);
        return result;

    }

    // Category logic provider
    async addSongToCategory(categoryId, lstSongId = [], userId = null) {
        let categoryObj = await this.getCategoryById(categoryId);
        if (!categoryObj) {
            return [];
        }
        let lstSongCreate = [];
        for (let i = 0; i < lstSongId.length; i++) {
            let obj = {
                song_id: lstSongId[i],
                category_id: categoryObj.id,
                creator: userId
            };
            let resultCreate = await songCategory.create(obj).then(val => val).catch(err => null);
            lstSongCreate.push(resultCreate);
        }
        return lstSongCreate;
    }

    async rmSongFromCategory(categoryId, lstSongId = []) {
        let categoryObj = await this.getCategoryById(categoryId);
        if (!categoryObj) {
            return [];
        }
        for (let i = 0; i < lstSongId.length; i++) {
            await songCategory.destroy({
                where: {
                    category_id: categoryObj.id,
                    $and: [{
                        song_id: {
                            $eq: lstSongId[i]
                        }
                    }]
                }
            });
        }
        return true;
    }

    async createCategory(categoryObject, lstSongId = [], userId = null) {
        let categoryObj = await category.create(categoryObject)
            .then(val => val)
            .catch(err => null);
        if (!categoryObj) {
            return null;
        }
        await this.addSongToCategory(categoryObj.id, lstSongId, userId);
        return categoryObj;
    }

    async updateCategory(categoryId, updateCategoryObj) {
        let categoryObj = await this.getCategoryById(categoryId);
        if (!categoryObj) {
            return null;
        }
        //Allow only update field if they not null, it's null keep old value
        // If can change password, then this user is active user.
        let updateField = LibUtils.updateObj(
            categoryObj,
            updateCategoryObj,
            [],
            ["id", "image"]
        );

        let result = await category
            .update(updateField, {
                where: {
                    id: categoryObj.id,
                },
                limit: 1,
            })
            .then(async val => {
                let fetchObj = await this.getCategoryById(categoryId);
                return  fetchObj;
            })
            .catch(err => null);
        return result;
    }


}

module.exports = new EntertainmentProvider();
