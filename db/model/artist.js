/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('artist', {
        'id': {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            primaryKey: true,
            comment: "null",
            autoIncrement: true
        },
        'firstname': {
            type: DataTypes.STRING(100),
            allowNull: true,
            comment: "null"
        },
        'lastname': {
            type: DataTypes.STRING(100),
            allowNull: true,
            comment: "null"
        },
        'creator': {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            comment: "null",
            references: {
                model: 'user',
                key: 'id'
            }
        },
        'created': {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
            comment: "null"
        }
    }, {
        tableName: 'artist'
    });
};
