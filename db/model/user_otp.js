/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('user_otp', {
        'id': {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            primaryKey: true,
            comment: "null",
            autoIncrement: true
        },
        'user_id': {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            comment: "null",
            references: {
                model: 'user',
                key: 'id'
            }
        },
        'code': {
            type: DataTypes.STRING(100),
            allowNull: true,
            comment: "null"
        },
        'created': {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
            comment: "null"
        },
        'updated': {
            type: DataTypes.DATE,
            allowNull: true,
            comment: "null"
        }
    }, {
        tableName: 'user_otp'
    });
};
