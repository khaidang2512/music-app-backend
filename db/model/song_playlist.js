/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('song_playlist', {
        'id': {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            primaryKey: true,
            comment: "null",
            autoIncrement: true
        },
        'song_id': {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            comment: "null",
            references: {
                model: 'song',
                key: 'id'
            }
        },
        'playlist_id': {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            comment: "null",
            references: {
                model: 'playlist',
                key: 'id'
            }
        },
        'creator': {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            comment: "null",
            references: {
                model: 'user',
                key: 'id'
            }
        },
        'created': {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
            comment: "null"
        }
    }, {
        tableName: 'song_playlist'
    });
};
