/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('role_permission', {
        'id': {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            primaryKey: true,
            comment: "null",
            autoIncrement: true
        },
        'role_id': {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            comment: "null",
            references: {
                model: 'role',
                key: 'id'
            }
        },
        'permission_id': {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            comment: "null",
            references: {
                model: 'permission',
                key: 'id'
            }
        }
    }, {
        tableName: 'role_permission'
    });
};
