/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('playlist', {
        'id': {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            primaryKey: true,
            comment: "null",
            autoIncrement: true
        },
        'name': {
            type: DataTypes.STRING(100),
            allowNull: false,
            comment: "null"
        },
        'creator': {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            comment: "null",
            references: {
                model: 'user',
                key: 'id'
            }
        },
        'created': {
            type: DataTypes.DATE,
            allowNull: true,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
            comment: "null"
        },
        'updater': {
            type: DataTypes.INTEGER(11),
            allowNull: true,
            comment: "null",
            references: {
                model: 'user',
                key: 'id'
            }
        },
        'updated': {
            type: DataTypes.DATE,
            allowNull: true,
            comment: "null"
        },
        'is_public': {
            type: DataTypes.INTEGER(1),
            allowNull: false,
            defaultValue: '1',
            comment: "null"
        },
        'image': {
            type: DataTypes.TEXT,
            allowNull: true,
            comment: "null"
        }
    }, {
        tableName: 'playlist'
    });
};
