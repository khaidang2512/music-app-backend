/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('user', {
        'id': {
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            primaryKey: true,
            comment: "null",
            autoIncrement: true
        },
        'email': {
            type: DataTypes.STRING(100),
            allowNull: false,
            comment: "null"
        },
        'password': {
            type: DataTypes.STRING(100),
            allowNull: false,
            comment: "null"
        },
        'firstname': {
            type: DataTypes.STRING(100),
            allowNull: true,
            comment: "null"
        },
        'lastname': {
            type: DataTypes.STRING(100),
            allowNull: true,
            comment: "null"
        },
        'age': {
            type: DataTypes.STRING(100),
            allowNull: true,
            comment: "null"
        },
        'birthday': {
            type: DataTypes.DATE,
            allowNull: true,
            comment: "null"
        },
        'address': {
            type: DataTypes.STRING(100),
            allowNull: true,
            comment: "null"
        },
        'image': {
            type: DataTypes.TEXT,
            allowNull: true,
            comment: "null"
        },
        'is_active': {
            type: DataTypes.INTEGER(1),
            allowNull: false,
            defaultValue: '0',
            comment: "null"
        }
    }, {
        tableName: 'user'
    });
};
