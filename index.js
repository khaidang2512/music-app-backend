"use strict";

require("dotenv-safe").config({
    allowEmptyValues: true,
    example: process.env.CI ? ".env.ci.example" : ".env.example",
});
const binaryMimeTypes = [
    'application/javascript',
    'application/json',
    'application/octet-stream',
    'application/xml',
    'audio/mp3',
    'audio/ogg',
    'audio/wav',
    'font/eot',
    'font/opentype',
    'font/otf',
    'image/jpeg',
    'image/png',
    'image/jpg',
    'image/svg+xml',
    'text/comma-separated-values',
    'text/css',
    'text/html',
    'text/javascript',
    'text/plain',
    'text/text',
    'text/xml'
]
const awsServerlessExpress = require("aws-serverless-express");
const app = require("./app");
const server = awsServerlessExpress.createServer(app, null, binaryMimeTypes);

const handler = (exports.handler = (event, context) => {
    awsServerlessExpress.proxy(server, event, context);
});
