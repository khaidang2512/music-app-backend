"use strict";

require("dotenv-safe").config({
    allowEmptyValues: true,
    example: process.env.CI ? ".env.ci.example" : ".env.example",
});

const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const morgan = require("morgan");
const logger = require("./lib/winston");
const os = require("os");
const cluster = require("cluster");
const cors = require("cors");

const LibUtils = require("./lib/LibUtils");

//Routing modules
const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");
const authRouter = require("./routes/auth");
const entertainmentRouter = require("./routes/entertainment");

const port = process.env.PORT || "3000";
let app;

//Speed up nodejs multi core
let multiHandler = (app) => {
    // combined, common, dev, short, tiny
    app.use(
        morgan(
            '[HTTP] (:response-time ms) :remote-addr - :remote-user [:date] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent"',
            {
                stream: logger.stream,
            }
        )
    );
    app.use(cors());
    app.use(express.json());
    app.use(express.urlencoded({extended: false}));
    app.use(cookieParser());
    app.use(express.static(path.join(__dirname, "public")));

    app.use("/", indexRouter);
    app.use("/users", usersRouter);
    app.use("/auth", authRouter);
    app.use("/entmt",entertainmentRouter);

    // catch 404 and forward to error handler
    app.use(async (req, res, next) => {
        return res.status(404).json({
            success: false,
            data: {
                status: "404 PAGE NOT FOUND",
                timestamp: new Date(),
                message: "Page not found",
            },
        });
    });

    // error handler
    app.use(function (err, req, res, next) {
        // set locals, only providing error in development
        res.locals.message = err.message;
        res.locals.error = req.app.get("env") === "development" ? err : {};

        // render the error page
        // res.status(err.status || 500)
        return res.status(err.status || 500).json({
            success: false,
            data: {
                status: LibUtils.statusCode(err.status) || LibUtils.statusCode(500),
                timestamp: new Date(),
                message: err.message || "Something wrong happen",
            },
        });
    });
    //For local testing api
    app.listen(port, () => {
        logger.info(
            "Listening on http://localhost:" +
            port +
            " and worker " +
            process.pid
        );
    });
};

const clusterWorkerSize = os.cpus().length;

if (clusterWorkerSize > 1) {
    if (cluster.isMaster) {
        for (let i = 0; i < clusterWorkerSize; i++) {
            cluster.fork();
        }

        cluster.on("exit", function (worker) {
            logger.info("Worker", worker.id, " has exited.");
        });
    } else {
        app = express();
        multiHandler(app);
    }
} else {
    const app = express();
    multiHandler(app);
}

module.exports = app;
